function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="col-md-4 mb-4">
        <div class="card" style="box-shadow: 7px 8px 8px 0px rgba(117,117,117,1);">
          <img src="${pictureUrl}" class="card-img-top" style="max-width: 100%;">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
            <h6 class="card-dates">${startDate} - ${endDate}</h6>
          </div>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("something broke...")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');
            console.log(details)
            console.log(location)
            row.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error(e);
        console.log("an error was caught with that thing learn told us to add")
      }


  });